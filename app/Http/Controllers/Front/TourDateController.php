<?php

namespace ckeditor\Http\Controllers\Front;

use Illuminate\Http\Request;
use ckeditor\Http\Controllers\Controller;

class TourDateController extends Controller
{
    /**
     * return Tour Date Page
     * @params : No params
     */
    public function index()
    {
        return view('front.pages.tour-dates');

    }
}
