<?php

namespace ckeditor\Http\Controllers\Admin;

use Illuminate\Http\Request;
use ckeditor\Http\Controllers\Controller;
use ckeditor\Models\Singers;

class SingerController extends Controller
{
    /**
     * Get list member is activated
     *
     * @params Request $request
     * @return $this
     */
    public function index(Request $request)
    {
        $result = Singers::select('id', 'realName', 'stageName', 'address', 'phone')->get();

        return view('admin.singer.listSinger')->with('result', $result);
    }

    public function addSinger(Request $request)
    {


    }

    /**
     * Get detail Information of Singer or actor
     * @params  singer_id
     * @output detailInfo
     */
    public function singerDetail($singer_id)
    {
        if (is_numeric($singer_id)) {
            $basicInfo = Singers::with(['company', 'musicType'])
                ->where('id', $singer_id)
                ->get();
//        var_dump($basicInfo); die;
            return view('admin.singer.detail')->with('result', $basicInfo);
        }
    }
}
