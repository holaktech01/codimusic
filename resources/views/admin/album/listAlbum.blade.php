@extends('layouts.admin')

@section('title', 'List Singers')
@section('description', 'This is a blank page that needs to be implemented')

@section('content')
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    Admin <small>Album</small>
                </h1>
                <ol class="breadcrumb">
                    <li class="active">
                        <i class="fa fa-dashboard"></i>Danh sach Album
                    </li>
                </ol>
                @include('notifications.status_message')
                @include('notifications.errors_message')
            </div>
        </div>
        <!-- /.row -->
        <button class="btn btn-primary">Thêm Album</button>
        <hr>
        <div class="row">
            <div class="col-lg-12">
                <table class="table">
                    <thead>
                        <td>STT</td>
                        <td>Album</td>
                        <td>Ngày phát hành</td>
                    </thead>
                    <tbody>
                    @foreach($result as $i => $member)
                        <tr>
                            <td>{{$i + 1}}</td>
                            <td>{{$member['name']}}</td>
                            <td>{{$member['publicTime']}}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->
@endsection