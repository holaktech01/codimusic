<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="application-name" content="Codi Music">
        <meta name="keywords" content="codi music">
        <meta name="author" content="Vu Dinh Ba">
        <meta name="description" content="Best beautiful web which i have coded">

        <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-theme.min.css">

        <script type="application/javascript" src="/bootstrap/js/jquery.min.js"></script>
        <script type="application/javascript" src="/bootstrap/js/bootstrap.min.js"></script>

        <title>Hoc Bootstrap</title>
    </head>
    <body>
        <div class="container">
            <hr>
            <!-- IMAGE test kieu web quang cao-->
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3 col-xs-12">
                        <img src="https://i.ytimg.com/vi/_rdtQvaYNWI/maxresdefault.jpg" class="img-responsive"
                             alt="responsive image">
                        <br>
                        <img src="https://i.ytimg.com/vi/JGDqm-Bdll4/maxresdefault.jpg" class="img-responsive"
                             alt="responsive image">
                        <br>
                        <img src="https://i.ytimg.com/vi/_rdtQvaYNWI/maxresdefault.jpg" class="img-responsive"
                             alt="responsive image">
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <!-- Post Content -->

                        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut,
                            error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae
                            laborum minus inventore?</p>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, tenetur natus doloremque laborum quos iste
                            ipsum rerum obcaecati impedit odit illo dolorum ab tempora nihil dicta earum fugiat. Temporibus,
                            voluptatibus.</p>

                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, doloribus, dolorem iusto blanditiis unde
                            eius illum consequuntur neque dicta incidunt ullam ea hic porro optio ratione repellat perspiciatis.
                            Enim, iure!</p>

                        <blockquote class="blockquote">
                            <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a
                                ante.</p>
                            <footer class="blockquote-footer">Someone famous in
                                <cite title="Source Title">Source Title</cite>
                            </footer>
                        </blockquote>
                        <div class="col-md-12">
                            <p class="bg-dark">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            <p class="bg-danger">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            <p class="bg-primary">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            <p class="bg-info">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                            <p class="bg-success">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>

                        </div>

                    </div>

                    <div class="col-md-3 col-xs-12">
                        <img src="https://i.ytimg.com/vi/_rdtQvaYNWI/maxresdefault.jpg" class="img-responsive"
                             alt="responsive image">
                        <br>
                        <img src="https://i.ytimg.com/vi/JGDqm-Bdll4/maxresdefault.jpg" class="img-responsive"
                             alt="responsive image">
                        <br>
                        <img src="https://i.ytimg.com/vi/_rdtQvaYNWI/maxresdefault.jpg" class="img-responsive"
                             alt="responsive image">

                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- End div -->
            <hr>
            <!-- Modal form mau -->
            <div class="row">
                <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Tìm kiếm</button>
                <div class="modal fade" id="myModal" role="dialog" tabindex="-1" aria-labelledby="My Modal is best thing" >
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3>Modal dau tien</h3>
                            </div>
                            <div class="modal-body">
                                <p class="bg-primary">NGon thi vao day</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End div -->
            <hr>
            <!-- Dropdown -->
            <div class="row">
                <div class="dropdown">
                    <button id="myDropdown" type="button" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false" class="btn btn-primary dropdown-toggle">
                        Dropdown Trigger
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu " aria-labelledby="myDropdown">

                            <li class="">HTML</li>
                            <li class="">CSS</li>
                            <li class="">JAVASCRIPT</li>
                            <li class="">PHP</li>

                    </ul>
                </div>

            </div>
            <!-- End div -->
        </div>
    </body>
</html>
<script>
    $(document).ready(function(){
        $('#myDropdown').on('show.bs.dropdown', function () {
            // do something…
            alert('ok');
        });
    });
</script>